﻿namespace CalculatePi
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) ) {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.piTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.decimalsNud = new System.Windows.Forms.NumericUpDown();
            this.goBtn = new System.Windows.Forms.Button();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.currentPositionLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.decimalsNud)).BeginInit();
            this.SuspendLayout();
            // 
            // piTxt
            // 
            this.piTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.piTxt.Location = new System.Drawing.Point(2, 53);
            this.piTxt.Multiline = true;
            this.piTxt.Name = "piTxt";
            this.piTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.piTxt.Size = new System.Drawing.Size(565, 200);
            this.piTxt.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of decimal places";
            // 
            // decimalsNud
            // 
            this.decimalsNud.Location = new System.Drawing.Point(147, 17);
            this.decimalsNud.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.decimalsNud.Name = "decimalsNud";
            this.decimalsNud.Size = new System.Drawing.Size(120, 20);
            this.decimalsNud.TabIndex = 2;
            this.decimalsNud.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // goBtn
            // 
            this.goBtn.Location = new System.Drawing.Point(273, 14);
            this.goBtn.Name = "goBtn";
            this.goBtn.Size = new System.Drawing.Size(75, 23);
            this.goBtn.TabIndex = 3;
            this.goBtn.Text = "&Go";
            this.goBtn.UseVisualStyleBackColor = true;
            this.goBtn.Click += new System.EventHandler(this.goBtn_Click);
            // 
            // progress
            // 
            this.progress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progress.Location = new System.Drawing.Point(0, 256);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(567, 23);
            this.progress.TabIndex = 4;
            // 
            // currentPositionLbl
            // 
            this.currentPositionLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.currentPositionLbl.AutoSize = true;
            this.currentPositionLbl.Location = new System.Drawing.Point(460, 19);
            this.currentPositionLbl.Name = "currentPositionLbl";
            this.currentPositionLbl.Size = new System.Drawing.Size(13, 13);
            this.currentPositionLbl.TabIndex = 5;
            this.currentPositionLbl.Text = "0";
            this.currentPositionLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Current position:";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 279);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.currentPositionLbl);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.goBtn);
            this.Controls.Add(this.decimalsNud);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.piTxt);
            this.Name = "mainForm";
            this.Text = "CalculatePi";
            ((System.ComponentModel.ISupportInitialize)(this.decimalsNud)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox piTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown decimalsNud;
        private System.Windows.Forms.Button goBtn;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Label currentPositionLbl;
        private System.Windows.Forms.Label label2;
    }
}

