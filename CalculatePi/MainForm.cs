﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatePi
{
    public partial class mainForm : Form
    {
        private int digitsToCalc = 0;

        public mainForm()
        {
            InitializeComponent();
        }

        private void ShowProgress( string pi, int totalDigits, int digitsSoFar )
        {
            piTxt.Text = pi;
            currentPositionLbl.Text = digitsSoFar.ToString();
            progress.Maximum = totalDigits;
            progress.Value = digitsSoFar;
        }

        private void CalcPi( int digits )
        {
            var result = new StringBuilder("3.");

            // Show progress
            ShowProgress( result.ToString(), digits, 0 );

            if ( digits <= 0 ) return;


            result.Append( "3." );

            for ( var i = 0; i < digits; i += 9 ) {
                var ds = CalculatePi.CalculatePiDigits( i + 1 );
                var digitCount = Math.Min( digits - i, 9 );

                if ( ds.Length < 9 )
                    ds = $"{int.Parse( ds ):D9}";

                result.Append( ds.Substring( 0, digitCount ) );

                // Show progress
                ShowProgress( result.ToString(), digits, i + digitCount );
            }
        }

        private void goBtn_Click( object sender, EventArgs e )
        {
            digitsToCalc = (int) decimalsNud.Value;
            var piThread = new Thread( CalcPiThreadStart );
            piThread.Start();
        }

        void CalcPiThreadStart()
        {
            CalcPi( digitsToCalc );
        }
    }
}
